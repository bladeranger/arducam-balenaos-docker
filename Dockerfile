FROM balenalib/raspberrypi3

ENV INITSYSTEM on

RUN install_packages python git build-essential i2c-tools tmux libraspberrypi-bin vim strace python-pip python-setuptools python-wheel mmc-utils

# Currently fails on build due to 'Unable to determine if this system is a Raspberry Pi'
#RUN pip install picamera

WORKDIR /usr/src/app

COPY . .

RUN git clone https://github.com/WiringPi/WiringPi.git

WORKDIR /usr/src/app/WiringPi

RUN ./build

WORKDIR /usr/src/app

RUN git clone https://github.com/ArduCAM/MIPI_Camera

WORKDIR /usr/src/app/MIPI_Camera

RUN git config --global user.email "builder@bladeranger.com"
RUN git config --global user.name "Docker Builder"

WORKDIR /usr/src/app/MIPI_Camera/RPI

RUN make install

RUN make preview-camera0 && make list_format && make capture_raw
RUN mv list_format list_format.rp3 && mv capture_raw capture_raw.rp3 && mv preview-camera0 preview-camera0.rp3

# Apply patch, build for fin
WORKDIR /usr/src/app/MIPI_Camera

RUN git am /usr/src/app/arducam-patches/0001-capture_raw-preview-camera0-setup-for-Balena-Fin-v1..patch

WORKDIR /usr/src/app/MIPI_Camera/RPI

RUN make preview-camera0 && make list_format && make capture_raw
RUN mv list_format list_format.fin && mv capture_raw capture_raw.fin && mv preview-camera0 preview-camera0.fin

WORKDIR /usr/src/app

CMD ["bash", "start.sh"]
