#!/usr/bin/python

from datetime import datetime
from time import sleep


def main():
    print("End of Arducam presentation. That's all folks!")
    start = str(datetime.now())
    counter = 0
    while True:
        print("{} {}".format(start, counter))
        counter += 1
        sleep(60)


if __name__ == '__main__':
    main()
