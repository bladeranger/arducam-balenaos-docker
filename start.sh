#!/bin/bash

export LD_LIBRARY_PATH=/opt/vc/lib

echo "================================================================================"
echo "Start date"
date
echo "System from uname -a"
uname -a
echo "Video core version"
vcgencmd version
echo "Video core possible (i.e. for arducam / raspiraw) and found (i.e. raspistill/raspivid) cameras"
vcgencmd get_camera

echo "================================================================================"
vcdbg log msg

echo "================================================================================"
/opt/vc/bin/mmal_vc_diag camerainfo

echo "================================================================================"
gpio readall

echo "================================================================================"
echo 'I2C bus 0 (VC)'
i2cdetect -y 0

echo "================================================================================"
echo "enabling pin 44"
gpio write 44 1

echo "Sleeping 1 second for camera to start (if it was enabled by pin 44 set)"
sleep 1.0 # let the camera initialize - probably way too long

echo "================================================================================"
echo 'I2C bus 0 (VC) after raising pin 44'
i2cdetect -y 0

echo "================================================================================"
echo "Running raspivid in demo mode. Will preview for known camera chipsets (yes ov5647, no ov9281)"

# try raspivid - this works with ov5467 (i.e. the RPi camera, v1 I think)
# This will show an image for a few seconds when the ov5476 is connected
# (or any other chipset recognized by raspivid, untested)
/opt/vc/bin/raspivid -d


echo "================================================================================"
echo "Running arducam - will run both versions to work on both rpi 3 and fincm3"
# now try to run Arducam
cd /usr/src/app/MIPI_Camera/RPI
echo "================================================================================"
echo "FIN FIN FIN FIN FIN FIN FIN FIN FIN FIN FIN"
./list_format.fin
./capture_raw.fin
echo "================================================================================"
echo "RP3 RP3 RP3 RP3 RP3 RP3 RP3 RP3 RP3 RP3 RP3"
./list_format.rp3
./capture_raw.rp3
echo "================================================================================"
echo "last two created files - should have some non empty jpg if successful"
ls -ltr | tail -2

# now enter an endless loop
echo "================================================================================"
echo "Entering endless loop (nothing interesting here)"
cd /usr/src/app
python start.py
